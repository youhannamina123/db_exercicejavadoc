import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
/**
 * permet de lancer l application 
 * dans laquelle on pourra ajouter des objets
 * de type participant (avec les proprietes)  et 
 * qu'il se mettra dans un tableau afin de l'afficher
 * 
 * @author Mina Ghobrial
 *
 */
public class AppParticipants extends JFrame {
	/**identificateur serial*/
	private static final long serialVersionUID = 1L;
	/**content pane*/
	private JPanel contentPane;;
	/**nom*/
	private JTextField txtNom;
	/**age*/
	private JSpinner spnAge;
	/**nbPartici�nt*/
	private JLabel lblNbParticipants;
	/**par*/
	private ArrayList<Participant> tousLesParticipants;
	/**btn*/
	private JButton btnAfficherTout;

	/**
	 * lancer application (le main)
	 * @param args paramettre de commande de d�part
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AppParticipants frame = new AppParticipants();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * constructeur de l'application, cr�ation de l'interface et des �couteurs 
	 */
	public AppParticipants() {
		
		tousLesParticipants = new ArrayList<Participant>();
		setTitle("Exercice sur javadoc");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 361);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(173, 216, 230));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblInfoNom = new JLabel("Entrez le nom du participant:");
		lblInfoNom.setBounds(64, 42, 177, 14);
		contentPane.add(lblInfoNom);
		
		txtNom = new JTextField();
		txtNom.setBounds(231, 39, 128, 20);
		contentPane.add(txtNom);
		txtNom.setColumns(10);
		
		JLabel lblInfoAge = new JLabel("Entrez l'\u00E2ge du participant:");
		lblInfoAge.setBounds(64, 84, 177, 14);
		contentPane.add(lblInfoAge);
		
		spnAge = new JSpinner();
		spnAge.setModel(new SpinnerNumberModel(18, null, null, 1));
		spnAge.setBounds(298, 81, 61, 17);
		contentPane.add(spnAge);
		
		JButton btnCreerParticipant = new JButton("Cr\u00E9er un participant et l'ajouter \u00E0 la liste");
		btnCreerParticipant.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				creerEtGererUnparticipant(txtNom.getText(), (int)spnAge.getValue() );
			}
		});
		btnCreerParticipant.setBounds(10, 132, 402, 40);
		contentPane.add(btnCreerParticipant);
		
		lblNbParticipants = new JLabel("0");
		lblNbParticipants.setBounds(271, 210, 88, 14);
		contentPane.add(lblNbParticipants);
		
		JLabel lblInfoNbParticip = new JLabel("Nombre de participants cr\u00E9\u00E9s:");
		lblInfoNbParticip.setBounds(64, 210, 177, 14);
		contentPane.add(lblInfoNbParticip);
		
		btnAfficherTout = new JButton("Afficher tous les participants \u00E0 la console");
		btnAfficherTout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				afficherTousLesParticipants();
			}
		});
		btnAfficherTout.setBounds(10, 258, 402, 40);
		contentPane.add(btnAfficherTout);
	}
	/**
	 * permet de creer un participant afin 
	 * de l ajouter dans un tableau avec
	 * ses parametres
	 * @param nomSaisi Chaine du nom du participant 
	 * @param ageSaisi entier de l'age du participant
	 */
	private void creerEtGererUnparticipant(String nomSaisi, int ageSaisi) {
		Participant p = new Participant(nomSaisi, ageSaisi );
		tousLesParticipants.add(p);	//ajouter au tableau
		lblNbParticipants.setText( ""+ (1+Integer.parseInt(lblNbParticipants.getText())));
	}
	
	/**
	 * permet d'afficher les participants 
	 * avec leur details comme leur 
	 * nom et leur age 
	 */
	private void afficherTousLesParticipants() {
		System.out.println("\nVoici les participants:\n");
		for ( Participant p : tousLesParticipants) {
			System.out.println(p.getNom() + " " + p.getAge() + " ans");
		}
	}
	
}//fin classe
