/**
 * permet de creer un object
 * de type participant avec un age et un nom
 *
 * @author Mina Ghobrial
 *
 */
public class Participant {
	/**nom participant */
	private String nom;
	/**age participant*/
	private int age;
	/**
	 * creer un participant (constructeur)
	 * @param nom du participant	
	 * @param age du participant
	 */
	public Participant(String nom, int age) {
		this.nom = nom;
		this.age = age;;
	}
	/**
	 * permet d ajouter un an au participant
	 */
	public void  ajouterUnAn() {
		this.age = this.age + 1;
	}
	/**
	 * permet de calculer la valeur x2 
	 * de l age et de l obtenir
	 * @return age 2x
	 */
	public int doubleDeLAge() {
		int deuxFois;
		deuxFois = this.age*2;
		return( deuxFois );
	}
	/**
	 * permet de retourner le nom du 
	 * participant
	 * @return nom du participant
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * permet de modifier le nom du participant
	 * @param nom du participant
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * permet d avoir l age du 
	 * participant
	 * @return age du participant
	 */
	public int getAge() {
		return age;
	}
	/**
	 * permet de modifier l age du participant
	 * @param age du partcipant (edit)
	 */
	public void setAge(int age) {
		this.age = age;
	}

}
